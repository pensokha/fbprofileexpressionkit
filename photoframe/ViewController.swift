//
//  ViewController.swift
//  photoframe
//
//  Created by pen sokha on 24/6/18.
//  Copyright © 2018 pen sokha. All rights reserved.
//

import UIKit
import FBSDKProfileExpressionKit

class ViewController: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate{

    @IBOutlet weak var imgPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        imgPhoto.image = chosenImage
        imagePicker.dismiss(animated: true, completion: nil)
    }
    var imagePicker = UIImagePickerController()
    @IBAction func selectPhoto(_ sender: Any) {
        
        
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func SetProfile(_ sender: Any) {
        
        if FBSDKProfileExpressionSharer.isProfileMediaUploadAvailable() {
            //
            FBSDKProfileExpressionSharer.uploadProfilePicture(from: imgPhoto.image, metadata: nil)
            
        }
        else{
            
            var faceAppURL = URL(string: "https://itunes.apple.com/us/app/facebook/id284882215")!
            if UIApplication.shared.canOpenURL(faceAppURL) {
                
                UIApplication.shared.openURL(faceAppURL)
            }
            else{
                
                // no facebook
            }
        }
    }
    
}

